const { Router } = require("express");
const { Message, Chat } = require("../models/postgres");
const { Op } = require("sequelize");
const { createLog } = require("./Log");
const checkIsAdmin = require("../middlewares/checkIsAdmin");

const router = new Router();

router.post('/', async (req, res) => {
    try {
        const chat = await Chat.findOne({
            where: {
                userIds: { [Op.contains]: [req.query.userIds] }
            }
        });
        if (chat) {
            const result = await Message.create({
                ...req.body,
                chatId: chat.id
            });
            res.status(201).json(result);
        } else {
            await createLog("Chat not found", "LOW", 404, "client", req.url);
            res.status(404).send({ message: "This chat doesn't exists" })
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.get('/', async (req, res) => {
    try {
        const chat = await Chat.findOne({
            where: {
                userIds: { [Op.contains]: [req.query.userIds] }
            }
        });
        if (chat) {
            const result = await chat.getMessages({
                order: [["createdAt", "ASC"]]
            });
            res.status(200).json(result);
        } else {
            await createLog("Chat not found", "LOW", 404, "client", req.url);
            res.sendStatus(404);
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.post('/:chatId', async(req, res) => {
    console.log(req.body);

    try {
        const result = await Message.create(req.body);

        res.status(201).json(result);
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    } 
});

router.get('/:chatId', async(req, res) => {
    try {
        const messages = await Message.findAll({
            where: {
                chatId: req.params.chatId,
            }
        });
        
        if (messages) {
            res.status(200).json(messages);
        } else {
            await createLog("Chat not found", "LOW", 404, "client", req.url);
            res.sendStatus(404);
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    } 
});

router.put('/:id', async (req, res) => {
    try {
        const chat = await Chat.findOne({
            where: {
                userIds: { [Op.contains]: [req.query.userIds] }
            }
        });
        const message = await Message.findByPk(req.params.id);
        if(!chat) {
            await createLog("Chat not found", "LOW", 404, "client", req.url);
        } else {
            if(!chat.hasMessage(message)) {
                await createLog("Message not found", "LOW", 404, "client", req.url);
            } else {
                if(req.body.content) {
                    await message.update(req.body);
                    res.status(200).json(message);
                }
                else {
                    await message.update({
                        content: "This message has been deleted"
                    });
                    res.status(200).json(message);
                }
            }
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.post('/ban/:id', checkIsAdmin, async (req, res) => {
    try {
        const message = await Message.findByPk(req.params.id);
        if(!message) {
            await createLog("Message not found", "LOW", 404, "client", req.url);
            res.sendStatus(404);
        } else {
            const messageUpdated = await Message.update({
                content: "Content deleted by admin"
            });
            res.status(200).json(messageUpdated);
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

module.exports = router;
