const { Router } = require("express");
const checkIsAdmin = require("../middlewares/checkIsAdmin");
const Technology = require("../models/postgres/Technology");
const { Sequelize } = require("sequelize");
const { createLog } = require("./Log");

const router = new Router();

router.get('/', async (req, res) => {
    try {
        const result = await Technology.findAll();
        if(!result) {
            await createLog("Ressource not found", 'AVERAGE', 404, 'client', req.url);
        } else {
            return res.status(200).json(result);
        }
    } catch (e) {
        await createLog(e, 'HIGH', 500, 'server', req.url);
        res.sendStatus(500);
    }
});

router.post('/', checkIsAdmin, async (req, res) => {
    try {
        const existTechno = await Technology.findOne({
            where: {
                name: {
                    [Sequelize.Op.iLike]: req.body.name.trim()
                }
            }
        });
        if (!existTechno) {
            const result = await Technology.create(req.body);
            return res.status(201).json(result);
        } else {
            await createLog("Technology already exist", "LOW", 404, "client", req.url);
            res.status(404).json({ message: "Technology already exists" });
        }
    } catch(e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        return res.sendStatus(500);
    }
});

module.exports = router;
