const { Router } = require("express");
const { User, PendingRequests } = require("../models/postgres");
const { createLog } = require("./Log");

const router = new Router();

router.get('/', async (req, res) => {
    try {
        const user = User.findByPk(req.user.id);
        if(!user) {
            await createLog("User not found", "AVERAGE", 400, "client", req.url);
            res.sendStatus(400);
        } else {
            const result = await PendingRequests.findAll({
                where: {
                    userId: req.user.id
                }
            });
            return res.status(200).json(result);
        }
    } catch (e) {
        await createLog(e, "HIGH", 500, "server", req.url);
        console.error(e);
        return res.sendStatus(500);
    }
});

router.get('/sended-request', async (req, res) => {
    try {
        const user = User.findByPk(req.user.id);
        if(!user) {
            await createLog("User not found", "AVERAGE", 400, "client", req.url);
            res.sendStatus(400);
        } else {
            const result = await PendingRequests.findAll({
                where: {
                    friendId: req.user.id
                }
            });
            return res.status(200).json(result);
        }
    } catch (e) {
        await createLog(e, "HIGH", 500, "server", req.url);
        console.error(e);
        return res.sendStatus(500);
    }
});

router.post('/', async (req, res) => {
    try {
        const userFriend = await User.findByPk(req.body.userId);
        if(userFriend) {
            // friendId is the id of the one that sent the invite
            const request = await PendingRequests.create({
                status: req.body.status,
                userId: req.body.userId,
                friendId: req.user.id
            });
            res.status(201).json({response: request, user: req.user.id});
        } else {
            await createLog("User not found", "AVERAGE", 400, "client", req.url);
            res.status(400).send({ message: "User not found" })
        }
    } catch (e) {
        res.sendStatus(500);
        await createLog(e, "HIGH", 500, "server", req.url);
        console.error(e)
    }
});

router.put('/:id', async (req, res) => {
    try {
        const pendingRequest = await PendingRequests.findByPk(req.params.id);
        await pendingRequest.update(req.body);
        const user = await User.findByPk(pendingRequest.userId);
        const friend = await User.findByPk(pendingRequest.friendId);

        if(!user || !friend) {
            await createLog("User not found", "AVERAGE", 400, "client", req.url);
            res.sendStatus(400)
        } else {
            switch(req.body.status) {
                case "Accepted":
                    await user.addFriend(friend);
                    await friend.addFriend(user);
                    res.status(200).send({ message: "Invitation accepted" });
                    break;
                case "Refused":
                    res.status(200).send({ message: "Invitation refused" });
                    break;
                case "Canceled":
                    res.status(200).send({ message: "Invitation canceled" });
                    break;
                default:
                    await createLog("Status not valid", "AVERAGE", 404, "client", req.url);
                    res.sendStatus(404);
            }
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

module.exports = router;
