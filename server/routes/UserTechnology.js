const { Router } = require("express");
const { User, Technology, sequelize, UserTechnology } = require("../models/postgres");
const { Op } = require("sequelize");
const { createLog } = require("./Log");

const router = new Router();

router.post('/', async (req, res) => {
    try {
        const user = await User.findByPk(req.body.userId);
        if (user) {
            for(const technologyId of req.body.technologyIds) {
                const technology = await Technology.findByPk(technologyId)
                user.addTechnologies([technology]);
            }
            const result = await user.getTechnologies();
            res.status(201).json(result);
        } else {
            await createLog("User doesn't exists", "AVERAGE", 404, "client", req.url);
            res.sendStatus(404);
        }
    } catch (error) {
        console.log(error);
        await createLog(error, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.get('/', async (req, res) => {
    try {
        const user = await User.findByPk(req.user.id);
        if(user) {
            const result = await user.getTechnologies();
            return res.status(200).json(result);
        } else {
            await createLog("User doesn't exists", "AVERAGE", 404, "client", req.url);
        }
    } catch (error) {
        console.log(error);
        await createLog(error, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.put('/', async (req, res) => {
    try {
        const user = await User.findByPk(req.user.id);
        if(user) {
            await UserTechnology.destroy({
                where: {
                    userId: req.user.id
                }
            });
            for(const technologyId of req.body.technologyIds) {
                const technology = await Technology.findByPk(technologyId);
                await user.addTechnologies([technology]);
            }
            const result = await user.getTechnologies();
            res.status(200).json(result);
        } else {
            await createLog("User doesn't exists", "AVERAGE", 404, "client", req.url);
            res.sendStatus(404);
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

module.exports = router;
