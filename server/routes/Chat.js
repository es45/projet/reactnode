const { Router } = require("express");
const { Op } = require("sequelize");
const { Chat } = require("../models/postgres");
const { createLog } = require("./Log");

const router = new Router();


router.post('/', async (req, res) => {
    try {
        const result = await Chat.create(req.body);
        if(!result) {
            await createLog("Error while creating chat", "LOW", 404, "client", req.url);
        } else {
            res.status(201).json(result);
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.get('/', async (req, res) => {
    try {
        let userIds = [req.user.id];

        const chat = await Chat.findAll({
            where: {
                userIds: { [Op.contains]: [userIds] }
            }
        });

        if (chat) {
            res.status(201).json({message: "chats obtained", result: chat});
        } else {
            await createLog("Chats not found", "LOW", 201, "client", req.url);
            res.status(201).json({message: "no chats" , result: false});
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.get('/:userId', async (req, res) => {
    
    try {
        let userIds = [req.user.id, req.params.userId];

        const chat = await Chat.findOne({
            where: {
                userIds: { [Op.contains]: [userIds] }
            }
        });

        if (chat) {
            res.status(201).json({message: "chat exist", data: chat, result: true});
        } else {
            await createLog("Chat not found", "LOW", 201, "client", req.url);
            res.status(201).json({message: "chat not exist" , result: false});
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

module.exports = router;
