const { Router } = require("express");
const { User, Report, Message } = require("../models/postgres");
const checkIsAdmin = require("../middlewares/checkIsAdmin")
const { createLog } = require("./Log");
const router = new Router();

router.get('/', async (req, res) => {
    try {
        const result = await Report.findAll({
            where: {
                userId: req.user.id
            }
        });
        res.status(200).json(result);
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.get('/all', checkIsAdmin, async (req, res) => {
    try {
        const result = await Report.findAll();
        res.status(200).json(result);
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.get('/sent', async (req, res) => {
    try {
        const result = await Report.findAll({
            where: {
                writtenById: req.user.id
            }
        });
        if(!result) {
            await createLog("Error while finding report writtenById", "HIGH", 400, "client", req.url);

        } else {
            res.status(200).json(result);
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.post('/:id', async (req, res) => {
    try {
        const userGettingReported = await User.findByPk(req.params.id);
        if(userGettingReported) {
            const result = await Report.create(req.body);
            res.status(200).json(result);
        } else {
            await createLog("User getting reported not found", "AVERAGE", 404, "client", req.url);
            res.status(400).json({ message: "User not found" });
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.post('/message/:id', async (req, res) => {
    try {
        const message = await Message.findByPk(req.params.id);
        if(!message) {
            await createLog("Message not found", "AVERAGE", 404, "client", req.url);
        } else {
            const result = await Message.update({
                content: "Content deleted due to moderation"
            });
            res.status(200).json(result);
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.post('/ban/:id', async (req, res) => {
   try {
       const userTobBan = await User.findByPk(req.params.id);
       if(!userTobBan) {
           await createLog("User getting banned not found", "HIGH", 404, "client", req.url);
       } else {
           const result = await User.update({
               isBanned: true
           });
           res.status(200).json(result);
       }
   } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
   }
});

router.post('/status/:id', checkIsAdmin, async (req, res) => {
    try {
        const report = await Report.findByPk(req.params.id);
        if(!report) {
            await createLog("Report not found", "AVERAGE", 404, "client", req.url);
            res.sendStatus(400);
        } else {
            const result = await report.update({
                status: req.body.status
            });
            res.status(200).json(result);
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

module.exports = router;
