const { Router } = require("express");
const { Log } = require("../models/mongo");
const mongoose = require("mongoose");
const checkIsAdmin = require("../middlewares/checkIsAdmin");
const { where } = require("sequelize");
const router = new Router();

const formatError = (validationError) => {
    return Object.keys(validationError.errors).reduce((acc, key) => {
        acc[key] = validationError.errors[key].message;
        return acc;
    }, {});
};

router.post("/", checkIsAdmin, async (req, res) => {
    try {
        const result = await Log.create(req.body);
        res.status(201).json(result);
    } catch (error) {
        if (error instanceof mongoose.Error.ValidationError) {
            res.status(422).json(formatError(error));
        } else {
            res.sendStatus(500);
            console.error(error);
        }
    }
});

router.get("/", checkIsAdmin, async (req, res) => {
    try {
        const result = await Log.find();
        res.status(200).json(result);
    } catch (error) {
        console.error(error);
        res.sendStatus(500);
    }
});

router.delete("/:id", checkIsAdmin, async (req, res) => {

});

module.exports = router;

module.exports.createLog = async function(description, severity, code, application, url) {
    try {
        await Log.create({
            description: description,
            severity: severity,
            code: code,
            application: application,
            url: url,
            date: new Date()
        });
    } catch (error) {
        console.error(error)
        // if (error instanceof mongoose.Error.ValidationError) {
        //     res.status(422).json(formatError(error));
        // } else {
        //     res.sendStatus(500);
        //     console.error(error);
        // }
    }

};
