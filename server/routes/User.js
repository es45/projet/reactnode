const { Router } = require("express");
const { User, Post, Technology } = require("../models/postgres");
const { ValidationError } = require("sequelize");
const checkIsAdmin = require("../middlewares/checkIsAdmin");
const { createLog } = require("../routes/Log");

const router = new Router();

const formatError = (validationError) => {
    return validationError.errors.reduce((acc, error) => {
        acc[error.path] = error.message;
        return acc;
    }, {});
};

router.get("/me", async (req, res) => {
    try {
        const result = await User.findOne({
            where: {
                id: req.user.id
            }
        });

        if (!result) {
            await createLog("User not found", 'AVERAGE', 404, 'client', req.url);
            res.sendStatus(404);
        } else {
            res.json(result);
        }
    } catch (error) {
        await createLog(formatError(error), 'HIGH', 500, 'server', req.url);
        console.error(error);
        res.sendStatus(500);
    }
})

router.get("/", checkIsAdmin, async (req, res) => {
    try {
        const { page = 1, perPage = 10, ...criteria } = req.query;

        const result = await User.findAll({
            where: criteria,
            limit: perPage,
            offset: (page - 1) * perPage,
            include: [
                { model: Post, as: "posts", limit: 2, order: [["createdAt", "DESC"]] },
                { model: Technology, as: "technologies", through: "user_technology" }
            ],
        });
        res.json(result);
    } catch (error) {
        await createLog(formatError(error), 'HIGH', 500, 'server', req.url);
        res.sendStatus(500);
        console.error(error);
    }
});

router.post("/", checkIsAdmin, async (req, res) => {
    try {
        const result = await User.create(req.body);
        res.status(201).json(result);
    } catch (error) {
        if (error instanceof ValidationError) {
            await createLog(formatError(error), 'AVERAGE', 422, 'client', req.url);
            res.status(422).json(formatError(error));
        } else {
            await createLog(formatError(error), 'HIGH', 500, 'server', req.url);
            res.sendStatus(500);
            console.error(error);
        }
    }
});

router.get("/:id", async (req, res) => {
    try {
        const result = await User.findByPk(req.params.id);
        if (!result) {
            await createLog("User not found", 'AVERAGE', 404, 'client', req.url);
            res.sendStatus(404);
        } else {
            res.json(result);
        }
    } catch (error) {
        await createLog(formatError(error), 'HIGH', 500, 'server', req.url);
        console.error(error);
        res.sendStatus(500);
    }
});

router.put("/:id", async (req, res) => {
    try {
        const [nbLines, [result]] = await User.update(req.body, {
            where: {
                id: parseInt(req.params.id, 10),
            },
            returning: true,
        });
        if (!nbLines) {
            await createLog('Missing number lines', 'AVERAGE', 404, 'client', req.url);
            res.sendStatus(404);
        } else {
            res.json(result);
        }
    } catch (error) {
        console.log(error);

        if (error instanceof ValidationError) {
            await createLog(formatError(error), 'AVERAGE', 422, 'client', req.url);
            res.status(422).json(formatError(error));
        } else {
            await createLog(formatError(error), 'HIGH', 500, 'server', req.url);
            res.sendStatus(500);
            console.error(error);
        }
    }
});

router.delete("/:id", async (req, res) => {
    try {
        const nbLines = await User.destroy({
            where: {
                id: parseInt(req.params.id, 10),
            },
        });
        if (!nbLines) {
            await createLog(formatError(error), 'AVERAGE', 404, 'client', req.url);
            res.sendStatus(404);
        } else {
            res.sendStatus(204);
        }
    } catch (error) {
        await createLog(formatError(error), 'HIGH', 500, 'server', req.url);
        res.sendStatus(500);
        console.error(error);
    }
});

// Delete account by admin
router.delete('/delete-account/:id', checkIsAdmin, async (req, res) => {
    try {
        const userToDelete = await User.findByPk(req.params.id);
        if (!userToDelete) {
            await createLog(formatError(error), 'AVERAGE', 404, 'client', req.url);
            res.sendStatus(404);
        } else {
            const result = await User.update({
                email: "deleted-account@ecole.fr"
            });
            res.status(200).json(result);
        }
    } catch (error) {
        await createLog(formatError(error), 'HIGH', 500, 'server', req.url);
        res.sendStatus(500);
        console.error(error);
    }
});

module.exports = router;
