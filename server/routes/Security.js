const { Router } = require("express");
const { User } = require("../models/postgres");
const { ValidationError } = require("sequelize");
const bcryptjs = require("bcryptjs");
const { createToken, createConfirmationCode } = require("../lib/jwt");
const sendMail = require("../utils/sendMail");
const { createLog } = require("../routes/Log")
const router = new Router();

const formatError = (validationError) => {
    return validationError.errors.reduce((acc, error) => {
        acc[error.path] = error.message;
        return acc;
    }, {});
};

router.post("/register", async (req, res) => {
    try {
        const result = await User.create({
            ...req.body,
            confirmationCode: await createConfirmationCode(req.body.email)
        });
        await sendMail(result.dataValues);
        res.status(201).json(result);
    } catch (error) {
        console.error(error)
        if (error instanceof ValidationError) {
            await createLog(formatError(error), 'AVERAGE', 422, 'client', req.url);
            res.status(422).json(formatError(error));
        } else {
            await createLog(formatError(error), 'HIGH', 500, 'server', req.url);
            res.sendStatus(500);
            console.error(error);
        }
    }
});

router.post("/login", async (req, res) => {
    try {
        const result = await User.findOne({
            where: {
                email: req.body.email,
            },
        });
        if (!result) {
            await createLog("Email not found", 'LOW', 401, 'client', req.url);
            res.status(401).json(new Error("Email not found"));
            return;
        }
        if(result?.status !== 'Active') {
            res.status(401).json({ message: "Pending Account. Please Verify Your Email!" });
            return;
        }
        if (!(await bcryptjs.compare(req.body.password, result.password))) {
            await createLog("Password is incorrect", 'LOW', 401, 'client', req.url)
            res.status(401).json({
                password: "Password is incorrect",
            });
            return;
        }
        res.json({ user: result, token: await createToken(result) });
    } catch (error) {
        // await createLog(error, 'HIGH', 500, 'server', req.url);
        // console.error("error", error);
        res.sendStatus(500);
    }
});

router.get('/confirm/:confirmationCode', async (req, res) => {
    try {
        const user = await User.findOne({
            where: {
                confirmationCode: req.params.confirmationCode
            }
        });
        if(!user) {
            await createLog("User not found", "HIGH", 404, 'client', req.url);
            res.sendStatus(404);
        } else {
            const result = await user.update({
                status: "Active"
            });
            res.status(200).json(result);
        }
    } catch (e) {
        await createLog(e, "HIGH",500, 'server', req.url)
        res.sendStatus(500);
    }
});

router.post('/forget-password', async (req, res) => {
    try {
        await sendMail(req.user);
        res.status(200).send({ status: "ok"})
    } catch (e) {
        res.sendStatus(400);
    }
});

module.exports = router;
