const { Router } = require("express");
const { User } = require("../models/postgres")
const { createLog } = require("./Log");
const router = new Router();

router.get('/', async (req, res) => {
    try {
        const user = await User.findByPk(req.user.id);
        if(!user) {
            await createLog("User not found", "LOW", 404, "client", req.url);
        } else {
            const result = await user.getFriends();
            res.status(200).json(result);
        }
    } catch (e) {
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        const user = await User.findByPk(req.user.id);
        const friend = await User.findByPk(req.params.id);
        if (!user || !friend) {
            await createLog("User not found", "LOW", 404, "client", req.url);
        } else {
            const friendToDelete = await User.findByPk(req.params.id);
            if(user.hasFriend(friendToDelete)) {
                user.removeFriend(friendToDelete);
                friend.removeFriend(user);
            }
            res.status(202).json({ message: "Removed from friend list" });
        }
    } catch (e) {
        console.error(e);
        await createLog(e, "HIGH", 500, "server", req.url);
        res.sendStatus(500);
    }
});

router.post('/:id/report', async (req, res) => {
    try {

    } catch (e) {

    }
});

module.exports = router;
