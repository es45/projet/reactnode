const express = require("express");
const port = process.env.PORT || 3000;
const app = express();
const cors = require("cors");
app.use(cors());
require('dotenv').config();
const HttpCodesRouter = require("./routes/HttpCode");
const UserRouter = require("./routes/User");
const PostRouter = require("./routes/Post");
const SecurityRouter = require("./routes/Security");
const TechnologyRouter = require("./routes/Technology");
const UserTechnologyRouter = require("./routes/UserTechnology");
const FriendRouter = require("./routes/Friend");
const PendingRequestsRouter = require("./routes/PendingRequests");
const MessageRouter = require("./routes/Message");
const ChatRouter = require("./routes/Chat");
const ReportRouter = require("./routes/Report");
const LogRouter = require("./routes/Log");
const checkAuthentication = require("./middlewares/checkAuthentication");
app.use(express.json());

app.get("/", (req, res, next) => {
  res.send("Hello world!");
});

//app.use(HttpCodesRouter);
app.use(SecurityRouter);
app.use("/http-codes", HttpCodesRouter);
app.use("/users", checkAuthentication, UserRouter);
app.use("/posts", checkAuthentication, PostRouter);
app.use("/technologies", checkAuthentication, TechnologyRouter);
app.use("/user-technologies", checkAuthentication, UserTechnologyRouter);
app.use("/friends", checkAuthentication, FriendRouter);
app.use("/requests", checkAuthentication, PendingRequestsRouter);
app.use("/messages", checkAuthentication, MessageRouter);
app.use("/chat", checkAuthentication, ChatRouter);
app.use("/report", checkAuthentication, ReportRouter);
app.use("/log", checkAuthentication, LogRouter);

app.listen(port, () => console.log(`Server started ${port}`));
