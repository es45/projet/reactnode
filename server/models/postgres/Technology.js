const { DataTypes, Model, Sequelize } = require("sequelize");
const sequelize = require("./db");

class Technology extends Model {}

Technology.init(
    {
        id: {
            type: DataTypes.UUID,
            defaultValue: Sequelize.UUIDV4,
            allowNull: false,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        }
    },
    {
        sequelize,
        modelName: "technology",
        freezeTableName: true
    }
);

module.exports = Technology;
