const { Model, DataTypes, Sequelize } = require("sequelize");
const sequelize = require("./db");

class Report extends Model {}

Report.init({
    id: {
        type: DataTypes.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
    },
    writtenById: {
        type: DataTypes.UUID,
        allowNull: false,
        foreignKey: true
    },
    date: {
        type: DataTypes.STRING,
        allowNull: false
    },
    category: {
        type: DataTypes.STRING,
        allowNull: false
    },
    status: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "report",
    freezeTableName: true
});

module.exports = Report;
