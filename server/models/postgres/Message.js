const { Model, DataTypes, Sequelize } = require("sequelize");
const { User } = require("../postgres/index")
const sequelize = require("./db");
class Message extends Model {}

Message.init({
    id: {
        type: DataTypes.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
    },
    userId: {
        type: DataTypes.UUID,
        allowNull: false,
        foreignKey: true
    },
    content: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    sequelize,
    modelName: "message",
    freezeTableName: true
});

module.exports = Message;
