const { Model, DataTypes, Sequelize } = require("sequelize");
const sequelize = require("./db");

class Chat extends Model {}

Chat.init({
    id: {
        type: DataTypes.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
    },
    userIds: {
        type: DataTypes.ARRAY(DataTypes.STRING),
        allowNull: false
    }
}, {
    sequelize,
    modelName: "chat",
    freezeTableName: true
});

module.exports = Chat;
