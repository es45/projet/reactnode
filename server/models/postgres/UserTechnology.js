const { DataTypes, Model, Sequelize } = require("sequelize");
const sequelize = require("./db");

class UserTechnology extends Model {}

UserTechnology.init(
    {
        id: {
            type: DataTypes.UUID,
            defaultValue: Sequelize.UUIDV4,
            allowNull: false,
            primaryKey: true
        }
    },
    {
        sequelize,
        modelName: "userTechnology",
        freezeTableName: true
    }
);

module.exports = UserTechnology;
