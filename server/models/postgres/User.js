const { DataTypes, Model, Sequelize } = require("sequelize");
const sequelize = require("./db");
const bcryptjs = require("bcryptjs");

class User extends Model {}

User.init(
    {
        id: {
            type: DataTypes.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true,
            },
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: {
                    min: 6,
                    max: 255,
                },
            },
        },
        isAdmin: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false,
        },
        isBanned: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        status: {
            type: DataTypes.STRING,
            defaultValue: "Pending",
            allowNull: false
        },
        confirmationCode: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        firstname: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                len: {
                    min: 2,
                },
            },
        },
        faculty: {
            type: DataTypes.STRING,
            allowNull: false
        }
    },
    {
        sequelize,
        modelName: "user",
        freezeTableName: true
    }
);

User.addHook("beforeCreate", async (user) => {
    user.password = await bcryptjs.hash(user.password, await bcryptjs.genSalt());
});

User.addHook("beforeUpdate", async (user, { fields }) => {
    if (fields.includes("password")) {
        user.password = await bcryptjs.hash(
            user.password,
            await bcryptjs.genSalt()
        );
    }
});

module.exports = User;
