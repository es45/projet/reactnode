exports.sequelize = require("./db");
exports.User = require("./User");
exports.Post = require("./Post");
exports.PendingRequests = require("./PendingRequests");
exports.Technology = require("./Technology");
exports.Message = require("./Message");
exports.UserTechnology = require("./UserTechnology");
exports.Chat = require("./Chat");
exports.Report = require("./Report");

const { Post: PostMongo } = require("../mongo");

exports.User.hasMany(exports.Post);
exports.Post.belongsTo(exports.User);
exports.User.hasMany(exports.PendingRequests);
exports.PendingRequests.belongsTo(exports.User);
exports.Chat.hasMany(exports.Message);
exports.Message.belongsTo(exports.Chat);
exports.User.hasMany(exports.Report);
exports.Report.belongsTo(exports.User);

exports.User.belongsToMany(exports.Technology, {
  through: exports.UserTechnology,
  as: "technologies",
  foreignKey: "userId"
});

exports.Technology.belongsToMany(exports.User, {
  through: exports.UserTechnology,
  as: "users",
  foreignKey: "technologyId"
});

exports.User.belongsToMany(exports.User, {
  through: "friend",
  as: "users",
  foreignKey: "friendId"
});

exports.User.belongsToMany(exports.User, {
  through: "friend",
  as: "friends",
  foreignKey: "userId"
});

// exports.Technology.belongsToMany(exports.User, { through: exports.UserTechnology });
// exports.User.belongsToMany(exports.Technology, { through: exports.UserTechnology });

async function denormalizePost(post) {
  await PostMongo.deleteOne({ _id: post.id });
  await PostMongo.create(
    await exports.Post.findByPk(post.id, {
      include: [{ model: exports.User, as: "user" }],
    })
  );
}

exports.Post.addHook("afterCreate", denormalizePost);
exports.Post.addHook("afterUpdate", denormalizePost);

exports.Post.addHook("afterDestroy", async (post) => {
  await PostMongo.deleteOne({ _id: post.id });
});

exports.User.addHook("afterUpdate", async (user) =>
  Promise.all(user.posts.map((post) => denormalizePost(post)))
);

exports.User.addHook("afterDestroy", async (post) => {
  return Promise.all(
    user.posts.map((post) => PostMongo.deleteOne({ _id: post.id }))
  );
});
