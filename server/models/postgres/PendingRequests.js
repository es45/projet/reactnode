const { Model, DataTypes, Sequelize } = require("sequelize");
const sequelize = require("./db");

class PendingRequests extends Model {
}

const status = ["Pending", "Accepted", "Refused"];

PendingRequests.init({
    id: {
        type: DataTypes.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false
    },
    friendId: {
        type: DataTypes.UUID,
        foreignKey: true,
        allowNull: false
    },
    status: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            validateStatus(value) {
                return status.includes(value);
            }
        }
    }
}, {
    sequelize,
    modelName: "pendingRequest",
    freezeTableName: true
});

module.exports = PendingRequests;
