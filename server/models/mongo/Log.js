const mongoose = require("./db");

const LogSchema = new mongoose.Schema({
    description: String,
    severity: String,
    code: Number,
    application: String,
    url: String
});

const Log = new mongoose.model("log", LogSchema);

module.exports = Log;
