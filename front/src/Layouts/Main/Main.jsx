import React from 'react'
import { Route, Routes } from "react-router-dom";
import Login from "../../Pages/Login";
import Register, { RegisterForm, RegisterFormData, RegisterFormTechs } from "../../Pages/Register";
import PasswordForgot from "../../Pages/PasswordForgot";
import Friends from "../../Pages/Friends";
import Messages from "../../Pages/Messages";
import Profile from "../../Pages/Profile";
import User from "../../Pages/User";
import SearchFriends from '../../Pages/SearchFriends';
import Errors from '../../Pages/Errors';
import EditProfile from '../../Pages/EditProfile';
import Admin from "../../Pages/Admin/Admin";
import Logs from "../../Pages/Admin/Logs";
import Reports from "../../Pages/Admin/Reports";
import AdminProfile from "../../Pages/Admin/AdminProfile";
import AdminHome from "../../Pages/Admin/AdminHome";

const Main = () => {
    return (
        <div className="flex bg-blue-300 min-h-full w-full sm:pt-20">
            <Routes>
                <Route exact path="/" element={<Login/>}/>
                <Route exact path="/register" element={<Register/>}>
                    <Route index element={<RegisterForm/>}/>
                    <Route path="data" element={<RegisterFormData/>}/>
                    <Route path="tech" element={<RegisterFormTechs/>}/>
                </Route>
                <Route exact path="/admin" element={<Admin/>}>
                    <Route path="home" element={<AdminHome/>}/>
                    <Route path="logs" element={<Logs/>}/>
                    <Route path="reports" element={<Reports/>}/>
                    <Route path="profile" element={<AdminProfile/>}/>
                </Route>
                <Route exact path="/admin" element={<Admin/>}/>
                <Route exact path="/password-forgot" element={<PasswordForgot/>}/>
                <Route exact path="/friends" element={<Friends/>}/>
                <Route exact path="/search-friends" element={<SearchFriends/>}/>
                <Route exact path="/messages/:id" element={<Messages/>}/>
                <Route exact path="/messages" element={<Messages/>}/>
                <Route exact path="/edit-profile" element={<EditProfile/>}/>
                <Route exact path="/profile" element={<Profile/>}/>
                <Route exact path="/user/:id" element={<User/>}/>
                <Route exact path='/errors' element={<Errors/>}/>
                <Route path='*' element={<Errors/>}/>
            </Routes>
        </div>
    );
}

export default Main
