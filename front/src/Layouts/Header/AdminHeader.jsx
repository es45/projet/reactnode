import React from 'react';

const AdminHeader = () => {
    return (
        <div className='bg-red-600 h-[24px] fixed top-0 w-full'>
            <span className='text-sm text-white font-bold'>Connecté en admin</span>
        </div>
    )
};

export default AdminHeader;
