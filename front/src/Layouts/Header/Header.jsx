import React from 'react'

const Header = () => {
  return(
    <header className="w-full">
      <div className="bg-blue-800/40 h-20 absolute top-0 w-full sm:flex hidden">
        <span>Header</span>
      </div>
    </header>
  );
}

export default Header
