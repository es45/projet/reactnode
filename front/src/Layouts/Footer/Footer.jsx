import React from 'react'
import { Link } from "react-router-dom";

const Footer = () => {
    return (
        <footer className="w-full">
            <div className="opacity-100 w-full h-20 sm:flex hidden">
                <span>Footer</span>
            </div>
            <div className="opacity-100 bg-slate-800 w-full text-white">
                <ul className="grid grid-cols-4 place-items-stretch-full sm:hidden">
                    <li className="overflow-hidden place-content-center">
                        <Link className="" to="/messages">
                            <div className='h-16 p-2'>
                                <span className="material-symbols-outlined text-4xl">
                                    chat
                                </span>
                            </div>
                        </Link>
                    </li>
                    <li className=" overflow-hidden place-content-center ">
                        <Link className="" to="/friends">
                            <div className='h-full p-2'>
                                <span className="material-symbols-outlined text-4xl">
                                  group
                                </span>
                            </div>
                        </Link>
                    </li>
                    <li className=" overflow-hidden place-content-center ">
                        <Link className="" to="/search-friends">
                            <div className='h-full p-2'>
                                <span className="material-symbols-outlined text-4xl">
                                    person_add
                                </span>
                            </div>
                        </Link>
                    </li>
                    <li className="overflow-hidden place-content-center ">
                        <Link className="" to="/profile">
                            <div className='h-full p-2'>
                              <span className="material-symbols-outlined text-4xl">
                                  account_circle
                              </span>
                            </div>
                        </Link>
                    </li>
                </ul>
            </div>
        </footer>
    );
}

export default Footer
