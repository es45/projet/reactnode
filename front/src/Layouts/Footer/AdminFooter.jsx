import React from 'react'
import { Link } from "react-router-dom";

const AdminFooter = () => {
    return (
        <footer className="w-full fixed bottom-0">
            <div className="opacity-100 w-full h-20 sm:flex hidden">
                <span>Footer</span>
            </div>
            <div className="opacity-100 bg-slate-800 w-full text-white">
                <ul className="grid grid-cols-4 place-items-stretch-full sm:hidden">
                    <li className="overflow-hidden place-content-center">
                        <Link className="" to="/admin/home">
                            <div className='h-16 p-2'>
                                <span className="material-symbols-outlined text-4xl">
                                    home
                                </span>
                            </div>
                        </Link>
                    </li>
                    <li className="overflow-hidden place-content-center">
                        <Link className="" to="/admin/logs">
                            <div className='h-16 p-2'>
                                <span className="material-symbols-outlined text-4xl">
                                    admin_panel_settings
                                </span>
                            </div>
                        </Link>
                    </li>
                    <li className=" overflow-hidden place-content-center ">
                        <Link className="" to="/admin/reports">
                            <div className='h-full p-2'>
                                <span className="material-symbols-outlined text-4xl">
                                  priority_high
                                </span>
                            </div>
                        </Link>
                    </li>
                    <li className="overflow-hidden place-content-center ">
                        <Link className="" to="/admin/profile">
                            <div className='h-full p-2'>
                              <span className="material-symbols-outlined text-4xl">
                                  account_circle
                              </span>
                            </div>
                        </Link>
                    </li>
                </ul>
            </div>
        </footer>
    );
}

export default AdminFooter;
