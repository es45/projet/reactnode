import axios, { AxiosResponse } from "axios";

interface LoginResponse {
  data?: {
    email: string;
    id: string;
    token: string;
  };
  message?: string;
  status?: string;
  error?: string;
}

function login(email: string, password: string, server: string): Promise<AxiosResponse<LoginResponse>> {
  const loginEndpoint = `${server}/api/accounts/users/log_in`;

  const payload = {
    user: {
      email: email,
      password: password,
    },
  };

  const reqHeaders = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  return axios.post(loginEndpoint, JSON.stringify(payload), reqHeaders);
}
  

export function register() {

}

export function passwordReset() {

}

export function friendsList() {

}

export function addFriend() {

}

export function removeFriend() {

}

export function usersList() {

}

export function techsList() {

}

export function getFaculty() {

}

export function editUserList() {

}

export function banUser() {

}

export function getReports() {

}

export function reportUser() {

}

