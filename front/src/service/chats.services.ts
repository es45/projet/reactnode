import httpToken from "../Http/httpToken";

export const createChat = async (payload: { userIds: string[] }) => {
    try {
        const uri = '/chat'
        const res = await httpToken.post(uri, payload);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const getChat = async(userId: string) => {
    try {
        const uri = `/chat/${userId}`
        const res = await httpToken.get(uri);
        return res.data;
    } catch (e) {
        throw e.message;
    }
}

export const getChats = async() => {
    try {
        const uri = `/chat`
        const res = await httpToken.get(uri);
        return res.data.result;
    } catch (e) {
        throw e.message;
    }
}
