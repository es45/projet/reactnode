import http from "../Http/http";
import httpToken from "../Http/httpToken";

export const profile = async () => {
    try {
        const uri = '/users/me'
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const login = async (payload: { email: string, password: string }) => {
    try {
        const uri = '/login'
        const res = await http.post(uri, payload);
        localStorage.setItem('token', res.data.token);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const register = async (payload: { email: string, password: string, isAdmin: boolean, faculty: string, firstname: string }) => {
    try {
        const uri = '/register'
        const res = await http.post(uri, payload);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const passwordForget = async (payload: { email: string }) => {
    try {
        const uri = '/forget-password'
        const res = await http.post(uri, payload);
        return res;
    } catch (e) {
        throw e.message;
    }
}
