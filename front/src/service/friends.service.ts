import httpToken from "../Http/httpToken";

export const friends = async () => {
    try {
        const uri = '/friends'
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const deleteFriend = async (friendToDelete: string) => {
    try {
        const uri = `/friends/${friendToDelete}`
        const res = await httpToken.delete(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}
