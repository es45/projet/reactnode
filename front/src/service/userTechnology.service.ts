import httpToken from "../Http/httpToken";

export const userTechnologies = async () => {
    try {
        const uri = '/user-technologies'
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const addUserTechnologies = async (payload: { userId: string, technologyIds: string[] }) => {
    try {
        const uri = `/user-technologies`
        const res = await httpToken.post(uri, payload);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const updateUserTechnologies = async (payload: { technologyIds: string[] }) => {
    try {
        const uri = `/user-technologies`
        const res = await httpToken.put(uri, payload);
        return res;
    } catch (e) {
        throw e.message;
    }
}
