import httpToken from "../Http/httpToken";

// @ts-ignore
export const sendRequest = async (payload: { status: string, userId: string }) => {
    try {
        const uri = `/requests`
        const res = await httpToken.post(uri, payload);
        console.log(res);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const requests = async () => {
    try {
        const uri = `/requests`
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const sendedRequests = async () => {
    try {
        const uri = `/requests/sended-request`
        const res = await httpToken.get(uri);
        return res.data;
    } catch (e) {
        throw e.message;
    }
}

export const updateRequest = async (payload: { requestId: string, status: string }) => {
    try {
        const uri = `/requests/${payload.requestId}`
        const res = await httpToken.put(uri, { status: payload.status });
        return res;
    } catch (e) {
        throw e.message;
    }
}