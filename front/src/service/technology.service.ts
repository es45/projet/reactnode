import httpToken from "../Http/httpToken";

export const technology = async () => {
    try {
        const uri = '/technologies'
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const addTechnology = async (payload: { name: string }) => {
    try {
        const uri = `/technologies`
        const res = await httpToken.post(uri, payload);
        return res;
    } catch (e) {
        throw e.message;
    }
}
