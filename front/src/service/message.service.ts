import httpToken from "../Http/httpToken";

// @ts-ignore
export const messages = async (payload: { chatId: string }) => {
    try {
        const uri = `/messages/` + payload.chatId;
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const addMessage = async (payload: { userIds: string[], content: string, userId: string }) => {
    try {
        const uri = `/messages?userIds=${payload.userIds[0]}&userIds=${payload.userIds[1]}`
        const res = await httpToken.post(uri, { content: payload.content, userId: payload.userId });
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const sendMessage = async(payload: { content: string, chatId: string, userId: string}) => {
    try {
        const uri = `/messages/${payload.chatId}`
        const res = await httpToken.post(uri, payload);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const deleteMessage = async (payload: { content: string, userId: string, userIds: string[] }) => {
    try {
        const uri = `/messages?userIds=${payload.userIds[0]}&userIds=${payload.userIds[1]}`
        const res = await httpToken.put(uri, { content: payload.content, userId: payload.userId });
        return res;
    } catch (e) {
        throw e.message;
    }
}
