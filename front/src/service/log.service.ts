import httpToken from "../Http/httpToken";

export const logs = async () => {
    try {
        const uri = `/log`
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}
