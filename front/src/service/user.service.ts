import httpToken from "../Http/httpToken";

export const users = async () => {
    try {
        const uri = '/users'
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e;
    }
}

export const createUser = async (payload: { email: string, password: string, isAdmin: boolean, faculty: string, firstname: string }) => {
    try {
        const uri = '/users'
        const res = await httpToken.post(uri, payload);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const updateUser = async (payload: { userId: string, email: string, password: string, isAdmin: boolean, faculty: string, firstname: string }) => {
    try {
        const uri = `/users/${payload.userId}`
        const res = await httpToken.put(uri, payload);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const getUser = async (userId: string) => {
    try {
        const uri = `/users/${userId}`
        const res = await httpToken.get(uri);

        return res;
    } catch (e) {
        throw e.message;
    }
} 

export interface User {
    confirmationCode: string,
    createdAt: string,
    email: string,
    faculty: string,
    firstname: string,
    id: string,
    isAdmin: boolean,
    isBanned: boolean,
    password: string,
    status: string,
    updatedAt: string
}