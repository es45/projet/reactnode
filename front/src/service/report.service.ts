import httpToken from "../Http/httpToken";

export const reports = async () => {
    try {
        const uri = `/report`
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const adminReports = async () => {
    try {
        const uri = `/report/all`
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const sentReports = async () => {
    try {
        const uri = `/report/sent`
        const res = await httpToken.get(uri);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const postReport = async (payload: { userId: string, writtenById: string, status: string, description: string  }) => {
    try {
        const uri = `/report/${payload.userId}`
        const res = await httpToken.post(uri, payload);
        return res;
    } catch (e) {
        throw e.message;
    }
}

export const updateReportStatus = async (payload: { reportId: string, status: string  }) => {
    try {
        const uri = `/report/status/${payload.reportId}`
        const res = await httpToken.post(uri, {status: payload.status});
        return res;
    } catch (e) {
        throw e.message;
    }
}
