// @ts-ignore
import React, { Component, useState } from 'react'
import './App.css'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { BrowserRouter, useNavigate } from "react-router-dom";
import Main from "./Layouts/Main/Main";

export interface ContextValues {
    user?: User;
    logoutUser?: () => void;
    loginUser?: (id: number, email: string, token: string) => void;
    checkLogin?: () => boolean;
}

interface User {
    id: number;
    email: string;
    token: string;
}

export const saveUserSession = (id: number, email: string, token: string) => {
    if (!localStorage.getItem("token")) {
        localStorage.setItem("token", token);
    }
}

export const destroyUserSession = () => {
    if (localStorage.getItem("token")) {
        localStorage.removeItem("token");
    }
}

export const checkLogin = () => {
    const token = localStorage.getItem("token");
    return !!token;
}

const App = () => {
    const [user, setUser] = React.useState<User | null>();

    const logoutUser = () => {
        setUser(null);
        destroyUserSession();
    }

    const loginUser = (id: number, email: string, token: string) => {
        setUser({id: id, email: email, token: token});
        saveUserSession(id, email, token);
    }

    const checkLogged = () => {
        const session = localStorage.getItem("MyGES-Connect-Session");
        if (session) {
            const parsed = JSON.parse(session);
            if (parsed.id && parsed.email && parsed.token) {
                loginUser(parsed.id, parsed.email, parsed.token);
            }

            return true;
        }
        return false;
    }

    return (
        <div className="App bg-red-300">
            <BrowserRouter>
                <div className="flex min-h-screen w-full bg-green-400">
                    <Main/>
                </div>
            </BrowserRouter>
            <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    );
}

export default App

const AppContext = () => {

}
