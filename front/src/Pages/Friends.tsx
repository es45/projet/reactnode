import React, { useCallback } from 'react'
import Header from "../Layouts/Header/Header";
import Footer from "../Layouts/Footer/Footer";
import { Link, useNavigate } from "react-router-dom";
import { checkLogin } from '../App';
import { toast } from 'react-toastify';
import { friends } from '../service/friends.service';
import { requests, updateRequest } from '../service/request.service';
import { getUser, User } from '../service/user.service';
import { createChat, getChat } from '../service/chats.services';
import { profile } from '../service/security.service';

const Friends = () => {
    let navigate = useNavigate();
    const [friendsList, setFriendsList] = React.useState<any[]>([]);
    const [requestList, setRequestList] = React.useState<any[]>([]);
    const [currentUser, setCurrentUser] = React.useState<any>({});

    React.useEffect((() => {
        if ( !checkLogin() ) {
            navigate("/");
        } else {
            updateFriends();
        }
    }), []);

    const filter = () => {
        if (requestList) {
            let list = requestList;

            if (friendsList?.length) {
                list = list.filter(i => {
                    let result = false;

                    friendsList.forEach((friend) => {
                        if ( friend ) {
                            if( i.friendId === friend.id ) {
                                result = true;
                            }
                        }
                    });

                    return !result;
                });
            }

            setRequestList(list);
        }
    }


    const getUserInfos = useCallback(async (userId: string) => {
        try {
            const res = await getUser(userId);
            return res.data;

        } catch (e) {
            toast.error(e);
        }
    }, []);

    const getProfile = useCallback(async () => {
        try {
            const res = await profile();
            setCurrentUser(res.data);
        } catch (e) {
            toast.error(e);
        }
    }, []);

    const updateFriends = () => {
        getRequest()
        .then(() => {
            getFriends();
            getProfile();
        })
        .finally(() => {
            filter();
        });
    };

    const getFriends = useCallback(async () => {
        try {
            const res = await friends();
            setFriendsList(res.data);
        } catch (e) {
            toast.error(e);
        }
    }, []);

    const getRequest = useCallback(async () => {
        setRequestList([]);
        try {
            const res = await requests();
            //setRequestList(res.data);
            //console.log(await getUserInfos("05b6412b-d969-45d7-bf59-a0e31b824703"))
            let list = res.data.filter((d) => d.status === 'pending' );
            if (!requestList.length) {
                list.map(async request => {
                    setRequestList([...requestList, {request: request, user: await getUserInfos(request.friendId)}]);
                });
            }
            /*res.data.map(async request => {
               setRequestList([...requestList, {request: request ,  user: await getUserInfos(request.userId)}]) 
            });*/

        } catch (e) {
            toast.error(e);
        }
    }, []);

    const AcceptRequest = React.useCallback(async(requestId) => {
        try {
            const res = await updateRequest({requestId: requestId, status: "Accepted"});
            toast.success(res.data.message);
            updateFriends();
            //setRequestList(requestList?.filter((r) => r.id !== requestId));
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    const RefuseRequest = React.useCallback(async(requestId) => {
        try {
            const res = await updateRequest({requestId: requestId, status: "Refused"});
            toast.success(res.data.message);
            updateFriends();
            //setRequestList(requestList?.filter((r) => r.id !== requestId));
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    const sendMessage =  React.useCallback(async(currentUserId, userId) => {
        try {
            const res = await getChat(userId);
            //toast.success(res.data.message);
            if(!res.result) {
                let chat: any = await createChat({ userIds: [currentUserId, userId]});
                navigate("/messages/" + chat.id);
            } else {
                console.log(res.data);
                navigate("/messages/" + res.data.id);
            }
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    return (
        <div className='flex flex-col w-full bg-green-200 '>
            <Header/>
            <div className='flex flex-col h-full w-full bg-red-500/40 relative'>
                <div className='relative h-16 w-full bg-slate-700 text-2xl text-white font-bold p-3'>
                    Amis
                    <div className='absolute right-2 top-2'>
                        <Link to=''>
                            <span className="material-symbols-outlined text-3xl h-10 w-10 rounded-lg bg-slate-600 ">
                                add
                            </span>
                        </Link>
                    </div>
                </div>
                <div className='flex flex-col bg-slate-600 h-full text-left p-3 text-white '>
                    <div className=''>
                        <span className='font-semibold'>Nouvelle demande d'amis - {requestList ? requestList.length : "0"}</span>
                        {
                            requestList ? 
                                requestList.map(request => {
                                    return (
                                        <div className='flex relative' key={request.request.id}>
                                            <img className='h-12 w-12 rounded-full bg-white my-2'/>
                                            <div className='m-2 w-1/2 flex flex-col'>
                                                <span className='font-bold text-md'>{request.user.firstname}</span>
                                                <span className='text-sm text-slate-200'>{request.user.faculty}</span>
                                            </div>
                                            <div className='h-full my-2 flex gap-2 absolute right-0'>
                                                <button type="button" onClick={() => AcceptRequest(request.request.id)}>
                                                    <span className="material-symbols-outlined bg-green-800 hover:bg-green-600 rounded-full p-2">
                                                    check
                                                    </span>
                                                </button>
                                                <button type="button" onClick={() => RefuseRequest(request.request.id)}>
                                                    <span className="material-symbols-outlined bg-red-800 hover:bg-red-600 rounded-full p-2">
                                                    close
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    )
                                })
                            : ""
                        }
                        
                        <hr className='bg-slate-500 my-2'></hr>
                    </div>
                    <div>
                        <span className='font-semibold'>Amis - {friendsList ? friendsList.length : "0" }</span>
                        {
                            friendsList ? 
                                friendsList.map(friend => {
                                    return (
                                        <div className='flex relative' key={friend.id}>
                                            <img className='h-12 w-12 rounded-full bg-white my-2'/>
                                            <div className='m-2 w-2/3 flex flex-col'>
                                                <span className='font-bold text-md'>{friend.firstname}</span>
                                                <span className='text-sm text-slate-200'>{friend.faculty}</span>
                                            </div>
                                            <div className='h-full my-2 flex absolute right-0 gap-2'>
                                                <button type="button" onClick={() => sendMessage(currentUser.id, friend.id)}>
                                                    <span className="material-symbols-outlined bg-slate-800 rounded-full p-2">
                                                    chat_bubble
                                                    </span>
                                                </button>
                                                <button type="button">
                                                    <span className="material-symbols-outlined bg-slate-800 rounded-full p-2 font-bold">
                                                    more_vert
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    )
                                })
                            : ""
                        }
                        
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    );
}

export default Friends;
