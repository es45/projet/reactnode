import React from 'react'
import Header from "../Layouts/Header/Header";
import Footer from "../Layouts/Footer/Footer";
import { Link } from 'react-router-dom';

const Errors = () => {
  return (
    <div className='flex flex-col w-full'>
      <Header />
      <div className='flex flex-col h-full w-full bg-slate-600 place-content-center text-white font-bold text-4xl'>
        <div>ERREUR 404</div>
        <div>PAGE INDISPONIBLE</div>
      </div>
      <Footer />
    </div>
  );
}

export default Errors;