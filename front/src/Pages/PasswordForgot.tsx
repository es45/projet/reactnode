import React from 'react'
import { Link, useNavigate } from "react-router-dom";
import { checkLogin } from '../App';
import { toast } from "react-toastify";
import { passwordForget } from '../service/security.service';


const PasswordForgot = () => {
    let navigate = useNavigate();

    React.useEffect((() => {
        if ( !checkLogin() ) {
            navigate("/messages");
        }
    }), []);
    
    const [email, setEmail] = React.useState<string>("");

    const submitPasswordForgot = React.useCallback(async (payload) => {
        try {
            const res = await passwordForget(payload);
            console.log("res", res)
            navigate('/messages');
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);
    
    return (
        <div className="flex bg-gradient-to-b from-blue-600 w-full to-white min-h-screen place-content-center">
            <div className="flex flex-col w-2/3 place-content-center">
                <form>
                    <h2>Mot de passe oublié?</h2>
                    <div>
                        <label>Email</label>
                        <input name="email" onChange={(e) => {setEmail(e.target.value)}}/>
                    </div>
                    <button type="button" onClick={() => submitPasswordForgot({email})}>
                        Valider
                    </button>
                </form>
                <Link className="button" to="/register">Inscription</Link>
                <Link className="button" to="/">Connexion</Link>
            </div>
        </div>
    );
}

export default PasswordForgot;
