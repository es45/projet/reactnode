import { Link, useNavigate } from "react-router-dom";
import Header from "../Layouts/Header/Header";
import Footer from "../Layouts/Footer/Footer";
import React from "react";
import { checkLogin } from "../App";

const EditProfile = () => {
    let navigate = useNavigate();

    React.useEffect((() => {
        if ( !checkLogin() ) {
            navigate("/");
        }
    }), []);

    return (
        <div className='flex flex-col w-full bg-green-200 '>
            <Header/>
            <div className='flex flex-col h-full w-full bg-red-500/40 relative'>
                <div className='relative h-16 w-full bg-slate-700 text-2xl text-white font-bold p-3'>
                    Edit profil
                    <div className='absolute left-2 top-2'>
                        <Link to='/profile'>
                            <span className="material-symbols-outlined text-3xl h-10 w-10 rounded-lg bg-slate-600 ">
                                arrow_back
                            </span>
                        </Link>
                    </div>
                </div>
                <div className='flex flex-col bg-slate-600 h-full'>
                    <div className='flex p-3'>
                        <div>
                            <img className='h-20 w-20 bg-white rounded-full'/>
                        </div>
                        <div className='p-2 flex flex-col text-left'>
                            <span className='text-white text-xl font-bold'>pseudo</span>
                            <span className='text-slate-200'>nom prénom</span>
                        </div>
                    </div>
                    <div className='bg-slate-400 h-full m-4 text-left p-4 rounded-lg'>
                        <span className='font-bold text-white text-xl'>Mes technos</span>
                        <div className='flex'>
                            <div><img className='h-16 w-16 rounded-lg bg-white my-2'/></div>
                            <div className='p-3 text-xl text-white font-bold'><span>Nom de la techno</span></div>
                        </div>
                        <hr className=''></hr>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    );
}

export default EditProfile;
