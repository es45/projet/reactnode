import React, { useCallback, useState } from 'react'
import AdminHeader from "../../Layouts/Header/AdminHeader";
import AdminFooter from "../../Layouts/Footer/AdminFooter";
import { Outlet } from "react-router-dom";


const Admin = () => {
    return (
        <div className='flex flex-col w-full bg-white'>
            <AdminHeader />
            <div className='h-full pb-[64px]'>
                <Outlet />
            </div>
            <AdminFooter />
        </div>
    )
}

export default Admin;
