import React from 'react';

const AdminHome = () => {
    return (
        <div className='h-full flex justify-center items-center text-3xl font-bold'>Bonjour Admin Suprême</div>
    )
}

export default AdminHome;
