import React, { useCallback, useEffect, useState } from 'react'
import Header from "../../Layouts/Header/Header";
import AdminFooter from "../../Layouts/Footer/AdminFooter";
import { Outlet } from "react-router-dom";
import { logs } from "../../service/log.service";
import app from "../../App";

const Logs = () => {

    const [applicationLogs, setLog] = useState<[]>([]);
    const [severity, setSeverity] = useState<string>('');
    const [application, setApplication] = useState<string>('');
    let [filteredLogs, setFilteredLogs] = useState<[]>([]);

    useEffect(() => {
        async function fetchLogs() {
            const result = await logs();
            setLog(result.data);
            setFilteredLogs(result.data);
        }
        fetchLogs();
    }, []);

    useEffect(() => {
        let filterList = applicationLogs;
        if (severity) {
            // @ts-ignore
            filterList = filterList.filter((log: any) => {
                return log.severity === severity
            })
        }
        if (application) {
            // @ts-ignore
            filterList = filterList.filter((log: any) => {
                return log.application === application
            });
        }
        setFilteredLogs(filterList);
    }, [severity, application]);

    const logList = () => filteredLogs.length ? filteredLogs.map((log: any) => {
        return (
            <div key={log._id} className='w-full border p-2'>
                <p className='font-bold text-left'>id : {log._id}</p>
                <p className='italic text-left pl-3'>{log.date}</p>
                <div className='p-3'>
                    <div className='odd:bg-white even:bg-slate-200 flex justify-between p-2'>
                        <p>Application</p>
                        <p>{log.application}</p>
                    </div>
                    <div className='odd:bg-white even:bg-slate-200 flex justify-between p-2'>
                        <p>Code</p>
                        <p>{log.code}</p>
                    </div>
                    <div className='odd:bg-white even:bg-slate-200 flex justify-between p-2'>
                        <p>Severité</p>
                        <p>{log.severity}</p>
                    </div>
                    <div className='odd:bg-white even:bg-slate-200 flex justify-between p-2'>
                        <p>Description</p>
                        <p>{log.description}</p>
                    </div>
                    <div className='odd:bg-white even:bg-slate-200 flex justify-between p-2'>
                        <p>Url</p>
                        <p style={{ 'overflowWrap': 'anywhere' }}>{log.url}</p>
                    </div>
                </div>
            </div>
        )
    }) :  (<div>Pas de logs</div>);

    const filter = () => {
        return (
            <div className='w-full flex justify-around py-5 fixed top-[24px] bg-white'>
                <div>
                    <select onChange={(e) => setSeverity(e.target.value)} defaultValue={'DEFAULT'}>
                        <option value="DEFAULT" disabled hidden>Sévérité</option>
                        <option value="LOW">LOW</option>
                        <option value="AVERAGE">AVERAGE</option>
                        <option value="HIGH">HIGH</option>
                    </select>
                </div>
                <div>
                    <select onChange={(e) => setApplication(e.target.value)} defaultValue={'DEFAULT'}>
                        <option value="DEFAULT" disabled hidden>Application</option>
                        <option value="client">Client</option>
                        <option value="server">Serveur</option>
                    </select>
                </div>
            </div>
        )
    };

    return (
        <div className='pt-[88px]'>
            {filter()}
            {logList()}
        </div>
    )
}

export default Logs;
