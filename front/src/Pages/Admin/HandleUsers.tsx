import React from 'react'
import Header from "../../Layouts/Header/Header";
import AdminFooter from "../../Layouts/Footer/AdminFooter";
import { Outlet } from "react-router-dom";

const Admin = () => {
    return (
        <>
            <div className='flex flex-col w-full bg-green-200 '>
                <Header />
                <Outlet />
                <AdminFooter />
            </div>
        </>
    )
}

export default Admin;