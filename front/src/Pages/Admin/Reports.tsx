import React from 'react';
import { adminReports, updateReportStatus } from '../../service/report.service';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { checkLogin } from '../../App';

const Reports = () => {
    const [reportsList, setReportsList] = React.useState<any[]>([]);

    let navigate = useNavigate();

    const getReports = React.useCallback(async() => {
        try {
            const res = await adminReports();
            //toast.success(res.data.message);
            setReportsList(res.data);
            console.log(res.data);
        } catch (e) {
            console.log(e);
        }
    }, []);

    async function UpdateReport(id, status) {
        await updateReportStatus({reportId: id , status: status});
        await getReports();
    };

    React.useEffect(() => {
        if ( !checkLogin() ) {
            navigate("/");
        } else {
            getReports();
        }
    }, []);

    return (
        <>
            <div className='w-full border p-2'>
                {
                reportsList ? 
                    reportsList.sort().map(report => {
                        return (
                            <div key={report.id} className='w-full border p-2'>
                                <p className='font-bold text-left'>id : {report.id}</p>
                                <div className='p-3'>
                                    <div className='odd:bg-white even:bg-slate-200 flex justify-between p-2'>
                                        <p>Date</p>
                                        <p className='italic text-left pl-3'>{report.date}</p>
                                    </div>
                                </div>
                                <div className='p-3'>
                                    <div className='odd:bg-white even:bg-slate-200 flex justify-between p-2'>
                                        <p>Categorie</p>
                                        <p className='italic text-left pl-3'>{report.category}</p>
                                    </div>
                                </div>
                                <div className='p-3'>
                                    <div className='odd:bg-white even:bg-slate-200 flex justify-between p-2'>
                                        <p>Statut</p>
                                        <p className='italic text-left pl-3'>{report.status}</p>
                                    </div>
                                </div>
                                <div className='p-3'>
                                    <div className='odd:bg-white even:bg-slate-200 flex justify-between p-2'>
                                        <p>Description</p>
                                        <p>{report.description}</p>
                                    </div>
                                </div>
                                <div className='p-3'>
                                    <button className="mx-5 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" type="button" onClick={() => UpdateReport(report.id, 'Closed')}>Fermer</button>
                                    <button className="mx-5 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded" type="button">Bannir</button>
                                </div>
                            </div>
                        )
                    })
                : "Aucun signalement"
                }
            </div>
        </>
    )
}

export default Reports;