import React, { useCallback } from 'react'
import Header from "../Layouts/Header/Header";
import Footer from "../Layouts/Footer/Footer";
import { Link, useNavigate } from 'react-router-dom';
import { checkLogin, destroyUserSession } from '../App';
import { profile } from '../service/security.service';
import { toast } from 'react-toastify';
import { User, users } from '../service/user.service';

const Profile = () => {
    let navigate = useNavigate();
    
    const [user, setUser] = React.useState<User>();

    React.useEffect((() => {
        if ( !checkLogin() ) {
            navigate("/");
        } else {
            getProfile();
        }
    }), []);

    function logout() {
        destroyUserSession();
        navigate("/");
    }
    
    const getProfile = useCallback(async () => {
        try {
            const res = await profile();
            console.log(res);
            setUser(res.data);
        } catch (e) {
            toast.error(e);
        }
    }, []);

    return (
        <div className='flex flex-col w-full bg-green-200 '>
            <Header/>
            <div className='flex flex-col h-full w-full bg-red-500/40 relative'>
                <div className='relative h-16 w-full bg-slate-700 text-2xl text-white font-bold p-3'>
                    Mon profil
                    <div className='absolute right-2 top-2'>
                        <Link to='/edit-profile'>
                            <span className="material-symbols-outlined text-3xl h-10 w-10 rounded-lg bg-slate-600 ">
                                edit
                            </span>
                        </Link>
                    </div>
                </div>
                <div className='flex flex-col bg-slate-600 h-full'>
                    <div className='flex p-3'>
                        <div>
                            <img className='h-20 w-20 bg-white rounded-full'/>
                        </div>
                        <div className='p-2 flex flex-col text-left'>
                            <span className='text-white text-xl font-bold'>{user ? user.firstname : "none"} <span className='text-sm'>{user ? user.email : "none"}</span></span>
                            <span className='text-slate-200'>{user ? user.faculty : "none"}</span>
                        </div>
                    </div>
                    <div className='bg-slate-400 h-full m-4 text-left p-4 rounded-lg'>
                        <span className='font-bold text-white text-xl'>Mes technos</span>
                        <div className='flex'>
                            <div><img className='h-16 w-16 rounded-lg bg-white my-2'/></div>
                            <div className='p-3 text-xl text-white font-bold'><span>Nom de la techno</span></div>
                        </div>
                        <hr className=''></hr>
                    </div>
                </div>
                <button className='bg-red-600 text-white h-16' onClick={logout}>Se déconnecter</button>
            </div>
            <Footer/>
        </div>
    );
}

export default Profile;
