import React from 'react'
import { Link, Outlet, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { register } from "../service/security.service";

const Register = () => {
    return (
        <>
            <Outlet/>
        </>
    );
}

export default Register;

export const RegisterForm = () => {

    const navigate = useNavigate();

    const [email, setEmail] = React.useState<string>("");
    const [firstname, setFirstname] = React.useState<string>("");
    const [password, setPassword] = React.useState<string>("");
    const [confirmPassword, setConfirmPassword] = React.useState<string>("");
    const [faculty, setFaculty] = React.useState<string>("");

    const submitRegister = async (email: string, password: string, confirmPassword: string, firstname: string, faculty: string) => {
        try {
            const payload = {
                email: email,
                password: password,
                isAdmin: false,
                faculty: faculty,
                firstname: firstname,
            }

            console.log(payload);
            console.log( confirmPassword === password );

            if ( confirmPassword === password ) {
                const res = await register(payload);
                console.log("res", res);

                navigate('/messages');
            } else {
                toast.error("Mot de passe incorrecte");
            }
      } catch (e) {
            toast.error(e);
            console.log(e);
      }
    };

    return (
        <div className="flex w-full bg-gradient-to-b from-blue-600 to-white min-h-screen place-content-center">
            <div className="flex flex-col place-content-center">
                <form>
                    <h2>Inscription</h2>
                    <div>
                        <label>Email</label>
                        <input name="email" onChange={(e) => setEmail(e.target.value)}/>
                        <label>Nom Prénom</label>
                        <input name="firstname" onChange={(e) => setFirstname(e.target.value)}/>
                        <label>Mot de passe</label>
                        <input name="password" type="password" onChange={(e) => setPassword(e.target.value)}/>
                        <label>Confirmation de mot de passe</label>
                        <input name="confirm-password" type="password" onChange={(e) => setConfirmPassword(e.target.value)}/>
                        <label>Filière</label>
                        <select name="faculty" defaultValue="IW" onChange={(e) => setFaculty(e.target.value)}>
                          <option value="IW">IW</option>
                        </select>
                    </div>
                    <button type="button" onClick={() => submitRegister( email, password, confirmPassword, firstname, faculty )}>
                        S'inscrire
                    </button>
                    <Link className="link" to="/">Se connecter</Link>
                    <Link to="/password-forgot">Mot de passe oublié?</Link>
                </form>
            </div>
        </div>
    );
}

export const RegisterFormData = () => {
    return (
        <div className="flex bg-gradient-to-b from-blue-600 to-white min-h-screen place-content-center">
            <div className="flex flex-col w-2/3 place-content-center">
                <form>
                    <h2>Completer votre profil</h2>
                    <div>
                        <label>Photo de profil</label>
                        <input type="file"/>
                        <label>Pseudo</label>
                        <input/>
                        <label>Nom</label>
                        <input/>
                        <label>Prénom</label>
                        <input/>
                        <label>Date de naissance</label>
                        <input type="date"/>
                    </div>
                    <button>
                        S'inscrire
                    </button>
                    <Link className="link" to="/">Se connecter</Link>
                    <Link to="/password-forgot">Mot de passe oublié?</Link>
                </form>
            </div>
        </div>
    );
}

export const RegisterFormTechs = () => {
    return (
        <div className="flex bg-gradient-to-b from-blue-600 to-white w-full min-h-screen place-content-center">
            <div className="flex flex-col w-2/3 place-content-center">
                <form className=" place-content-center">
                    <h2>Choisissez vos techs</h2>
                    <div className="grid grid-cols-2 h-64">
                        <div className="tech-button tech-1"></div>
                        <input type="checkbox"/>
                        <div className="tech-button"></div>
                        <div className="tech-button"></div>
                        <div className="tech-button"></div>
                    </div>
                    <button>
                        Valider
                    </button>
                </form>
            </div>
        </div>
    );
}
