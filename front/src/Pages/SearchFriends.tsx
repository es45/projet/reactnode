import React, {useCallback} from 'react'
import Header from "../Layouts/Header/Header";
import Footer from "../Layouts/Footer/Footer";
import { Link, useNavigate } from 'react-router-dom';
import { checkLogin } from '../App';
import { getUser, User, users } from '../service/user.service';
import { toast } from 'react-toastify';
import { requests, sendedRequests, sendRequest, updateRequest } from '../service/request.service';
import { friends } from '../service/friends.service';
import { profile } from '../service/security.service';

const SearchFriends = () => {

    let navigate = useNavigate();
    const [allUsers, setAllUsers] = React.useState<User[]>();
    const [usersList, setUsersList] = React.useState<User[]>();
    const [requestList, setRequestList] = React.useState<any[]>();
    const [sendedRequestList, setSendedRequestList] = React.useState<any[]>([]);
    const [friendsList, setFriendsList] = React.useState<any[]>();
    const [currentUser, setCurrentUser] = React.useState<User>();
    const [filterUser, setFilterUser] = React.useState<string>();
    const [filterFaculty, setFilterFaculty] = React.useState<string>();

    React.useEffect((() => {
        if ( !checkLogin() ) {
            navigate("/");
        } else {
            updateUsers();
        }
    }), []);

    React.useEffect((() => {
        filter()
    }), [sendedRequestList, requestList, friendsList, currentUser]);

    const filter = () => {
        if (usersList) {
            let list = usersList;

            if (sendedRequestList?.length) {
                list = list.filter(i => {
                    let result = false;

                    sendedRequestList.forEach((sendedRequest) => {
                        if ( sendedRequest ) {
                            if( i.id === sendedRequest.userId ) {
                                result = true;
                            }
                        }
                    });

                    return !result;
                });
            }

            if (requestList?.length) {

                list = list.filter(i => {
                    let result = false;

                    requestList.forEach((request) => {
                        if ( request ) {
                            if( i.id === request.userId ) {
                                result = true;
                            }
                        }
                    })

                    return !result;
                });
            }

            if (friendsList?.length) {
                list = list.filter(i => {
                    let result = false;

                    friendsList.forEach((friend) => {
                        if ( friend ) {
                            if( i.id === friend.id ) {
                                result = true;
                            }
                        }
                    })

                    return !result;
                });
            }

            if (currentUser) {
                list = list.filter(i => i.id !== currentUser.id);
            }

            setUsersList(list);
        }
    }

    const updateUsers = () => {
        getUsers()
        .then(() => {
            getCurrentUser();
            getRequest();
            getSendedRequest();
            getFriends();
        })
        .finally(() => {
            filter();
        });
    };

    const getUserInfos = React.useCallback(async (userId: string) => {
        try {
            const res = await getUser(userId);
            return res.data;

        } catch (e) {
            toast.error(e);
        }
    }, []);

    const getCurrentUser = React.useCallback(async () => {
        try {
            const res = await profile();
            setCurrentUser(res.data);
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    const getFriends = React.useCallback(async () => {
        try {
            const res = await friends();
            setFriendsList(res.data)
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    const getUsers = React.useCallback(async () => {
        try {
            const res = await users();
            setUsersList(res.data)
            setAllUsers(res.data);
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    const sendFriendRequest = React.useCallback(async(userId) => {
        try {
            const res = await sendRequest({status: 'pending', userId: userId});
            updateUsers();
            //setUsersList(usersList?.filter((u) => u.id !== userId ));
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    const getRequest = React.useCallback(async() => {
        try {
            const res = await requests();
            setRequestList(res.data.filter((d) => d.status === 'pending' ));
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []); 

    const getSendedRequest = useCallback(async() => {
        try {
            const res = await sendedRequests();
            let list = res.filter((d) => d.status === 'pending' );
            setSendedRequestList(list);
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    const AcceptRequest = React.useCallback(async(requestId) => {
        try {
            const res = await updateRequest({requestId: requestId, status: "Accepted"});
            toast.success(res.data.message);
            updateUsers();
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    const RefuseRequest = React.useCallback(async(requestId) => {
        try {
            const res = await updateRequest({requestId: requestId, status: "Refused"});
            toast.success(res.data.message);
            updateUsers();
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    const cancelRequest = React.useCallback(async(requestId) => {
        try {
            const res = await updateRequest({requestId: requestId, status: "Canceled"});
            toast.success(res.data.message);
            updateUsers();
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);
    

    return (
        <div className='flex flex-col w-full bg-green-200 '>
            <Header/>
            <div className='flex flex-col h-full w-full bg-red-500/40 relative'>
                <div className='relative h-16 w-full bg-slate-700 text-2xl text-white font-bold p-3'>
                    Trouver un utilisateur
                </div>
                <div className='relative h-16 w-full bg-slate-700 text-2xl text-white font-bold p-3'>
                    <form className='h-full p-0 flex flex-row bg-transparent'>
                        <input className='rounded mr-2 p-1 w-full bg-slate-400 text-base'></input>
                        <button type="button" className="p-0 m-0 bg-transparent">
                <span className="material-symbols-outlined text-3xl h-10 w-10 rounded-lg bg-slate-600 ">
                  search
                </span>
                        </button>
                    </form>
                </div>
                <div className='flex flex-col bg-slate-600 h-full p-4 overflow-auto'>
                    <div>
                        <div className='text-left text-white'>
                            Demande d'amis
                        </div>
                        {
                            requestList ? 
                                requestList.map(request => {
                                    let user = allUsers?.find(u => u.id === request.friendId);

                                    return (
                                        <div className='flex relative' key={request.id} >
                                            <img className='h-12 w-12 rounded-full bg-white my-2'/>
                                            <div className='m-2 w-1/2 flex flex-col text-left text-white'>
                                                <span className='font-bold text-md'>{user?.firstname}</span>
                                                <span className='text-sm text-slate-200'>{user?.faculty}</span>
                                            </div>
                                            <div className='h-full flex gap-2 absolute right-0 text-white mb-3'>
                                                <button type="button" onClick={() => AcceptRequest(request.id)}>
                                                    <span className="material-symbols-outlined bg-green-800 hover-green-600 rounded-full p-2">
                                                        check
                                                    </span>
                                                </button>
                                                <button type="button" onClick={() => RefuseRequest(request.id)}>
                                                    <span className="material-symbols-outlined bg-red-800 hover:bg-red-600 rounded-full p-2">
                                                        close
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    )
                                })
                            : ""
                        }
                    </div>
                    <div>
                        <div className='text-left text-white'>
                            Demande envoyée
                        </div>
                        {
                            sendedRequestList ? 
                                sendedRequestList.map(sendedRequest => {
                                    let user = allUsers?.find(u => u.id === sendedRequest.userId);
                                    return (
                                        <div className='flex relative' key={sendedRequest.id} >
                                            <img className='h-12 w-12 rounded-full bg-white my-2'/>
                                            <div className='m-2 w-1/2 flex flex-col text-left text-white'>
                                                <span className='font-bold text-md'>{user?.firstname}</span>
                                                <span className='text-sm text-slate-200'>{user?.faculty}</span>
                                            </div>
                                            <div className='h-full flex gap-2 absolute right-0 text-white mb-3'>
                                                <button type="button" onClick={() => cancelRequest(sendedRequest.id)}>
                                                    <span className="material-symbols-outlined bg-red-800 hover:bg-red-600 rounded-full p-2">
                                                        close
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    )
                                })
                            : ""
                        }
                    </div>
                    
                    <div>
                        <div className='text-left text-white'>
                            Liste utilisateurs
                        </div>
                        {
                            usersList ? 
                                usersList.map(user => {
                                    return (
                                        <div className='flex relative' key={user.id} >
                                            <img className='h-12 w-12 rounded-full bg-white my-2'/>
                                            <div className='m-2 w-1/2 flex flex-col text-left text-white'>
                                                <span className='font-bold text-md'>{user.firstname} </span>
                                                <span className='text-sm text-slate-200'>{user.faculty}</span>
                                            </div>
                                            <div className='h-full my-2 flex gap-2 absolute right-0 text-white'>
                                                <button type="button" onClick={() =>sendFriendRequest(user.id)}>
                                                    <span className="material-symbols-outlined bg-slate-800 rounded-full p-2">
                                                        add
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    )
                                })
                            : ""
                        }
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    );
}

export default SearchFriends;
