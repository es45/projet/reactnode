import React, { useCallback, useRef } from 'react'
import Header from "../Layouts/Header/Header";
import Footer from "../Layouts/Footer/Footer";
import { checkLogin } from '../App';
import { useNavigate, useParams } from 'react-router-dom';
import { getChats } from '../service/chats.services';
import { getUser, User, users } from '../service/user.service';
import { profile } from '../service/security.service';
import { toast } from 'react-toastify';
import { messages, sendMessage } from '../service/message.service';
import Moment from 'moment';

const Messages = () => {
    Moment.locale('fr');
    let navigate = useNavigate();
    let { id } = useParams();

    const [messagesList, setMessagesList] = React.useState<any[]>();
    const [chatList, setChatList] = React.useState<any[]>();
    const [currentChat, setCurrentChat] = React.useState<any>();
    const [currentUser, setCurrentUser] = React.useState<any>();
    const [messageUser, setMessageUser] = React.useState<User>();
    const [message, setMessage] = React.useState<string>('');
    const [userList, setUserList] = React.useState<User[]>([]);
    const [messageEnd, setMessageEnd] = React.useState<any>();

    React.useEffect(() => {
        if ( !checkLogin() ) {
            navigate("/");
        } else {
            getProfile();
            updateChats();
            getUserList();
        }
    }, []);

    React.useEffect(() => {
        if (currentChat) {
            let userId = currentChat.userIds.filter((id) => currentUser?.id !== id)[0];
            getMessageUser(userId);
            getMessageList(currentChat.id)
            
            const timer = setInterval(() => getMessageList(currentChat.id), 500);
            return () => clearInterval(timer);
        }
    }, [currentChat]);

    React.useEffect(() => {
        updateChats();
    }, [id]);

    const getProfile = useCallback(async () => {
        try {
            const res = await profile();
            setCurrentUser(res.data);
        } catch (e) {
            toast.error(e);
        }
    }, []);

    const updateChats = () => {
        getAllChats().then(
            (chats) => {
                if(chats) {
                    let chat = chats.filter((c) => c.id === id);
                    setCurrentChat(chat[0]);
                }
            }
        );
    };
    
    function closeTab(_e) {
        let element = document.getElementById('messages-tab');
        if (element) {
            element.classList.add('-translate-x-full');
            element.classList.remove('translate-x-full');
        }
    };

    function openTab(_e) {
        let element = document.getElementById('messages-tab');
        if (element) {
            element.classList.add('translate-x-full');
            element.classList.remove('-translate-x-full');
        }
    }

    const getMessageList = React.useCallback(async(chatId) => {
        try {
            const res = await messages({ chatId: chatId });
            setMessagesList(res.data);

            //toast.success(res.data.message);
            //setMessageUser(res.data);
        } catch (e) {
            console.log(e);
        }
    }, []);

    const getMessageUser = React.useCallback(async(id) => {
        try {
            const res = await getUser(id);
            //toast.success(res.data.message);
            setMessageUser(res.data);
        } catch (e) {
            console.log(e);
        }
    }, []);

    const getAllChats = React.useCallback(async() => {
        try {
            const res = await getChats();
            setChatList(res);
            return res;
        } catch (e) {
            console.log(e);
        }
    }, []);

    const getUserList = React.useCallback(async() => {
        try {
            const res = await users();
            setUserList(res.data);
            //toast.success(res.data.message);
        } catch (e) {
            console.log(e);
        }
    }, []);

    return (
        <div className='flex flex-col w-full max-h-screen bg-green-200 '>
            <div className='flex flex-col h-full w-full bg-red-500/40 relative'>
                <Header/>
                <div className=' flex h-16 w-full bg-slate-700'>
                    <button className='h-full w-16' onClick={openTab}>
                        <span className="material-symbols-outlined text-4xl text-white">
                            menu
                        </span>
                    </button>
                    <div className='flex flex-col w-full text-xl text-white p-2 font-bold'>
                        <span>{messageUser?.firstname}</span>
                        <span className='text-sm text-slate-300'>{messageUser?.faculty}</span>
                    </div>
                </div>
                <div id='messages-tab'
                     className='absolute -left-full h-full w-full transition-transform duration-300 ease-in-out drop-shadow-2xl'>
                    <div className='w-10/12 bg-slate-700 h-full'>
                        <div className='flex h-12'>
                            <div className='h-full w-full text-white font-bold p-2'>
                                <span className='text-xl'>Tous les messages</span>
                            </div>
                            <button className='text-white h-12 w-12' onClick={closeTab}>
                                <span className="material-symbols-outlined">
                                    close
                                </span>
                            </button>
                        </div>
                        <div className='overflow-auto'>
                            {
                                chatList ? 
                                    chatList.map(chat => {
                                        let userId = chat.userIds.filter(u => u !== currentUser?.id)[0];
                                        let user = userList.find(u => u.id === userId);
                                        return (
                                            <div className='flex w-full bg-slate-800 p-4 hover:bg-slate-700' key={chat.id} onClick={(e) => {navigate("/messages/" + chat.id); closeTab(e) }}>
                                                <img className='h-16 w-16 rounded-full mr-3 bg-white'/>
                                                <div className='px-2 overflow-hidden w-2/3'>
                                                    <div className='flex flex-col w-full'>
                                                        <span className='font-bold text-white w-full text-left text-ellipsis overflow-hidden'>{user?.firstname}</span>
                                                        <span className='font-bold text-slate-300 w-full text-left text-ellipsis overflow-hidden'>{user?.faculty}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        );
                                    })
                                : ""
                            }
                            
                        </div>
                    </div>
                </div>
                <div className='bg-slate-600 p-4 h-full w-full overflow-auto text-white'>
                    { messagesList ?
                        messagesList.map((message, idx) => {

                            if ( idx === 0 ) {
                                return (
                                    <div className='flex flex-col mt-4' key={message.id}>
                                        <span className='text-sm text-white'>{Moment(message.createdAt).format('d MMM YYYY')}</span>
                                        <div className='flex mt-4'>
                                            <div>
                                                <div className="h-14 w-14">
                                                    <img className='h-14 w-14 bg-white rounded-full'/>
                                                </div>
                                            </div>
                                            <div className='flex flex-col w-full ml-3'>
                                                <div className='text-left mb-2'>
                                                    <span className='font-bold'>
                                                        { message.userId === messageUser?.id ? messageUser?.firstname : currentUser?.firstname }
                                                    </span> 
                                                    <span className='text-sm text-slate-300 mx-3'>{ message.userId === messageUser?.id ? messageUser?.faculty : currentUser?.faculty }</span>
                                                    <span className='text-sm text-slate-300'>{Moment(message.createdAt).format('HH:mm ')}</span>
                                                </div>
                                                <div>
                                                    <p className='text-left w-full'>
                                                        {message.content}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            } else if (messagesList[idx - 1].userId === message.userId) {
                                return (
                                    <div className='flex' key={message.id}>
                                        <div>
                                            <div className="w-14">
                                                <span className='text-sm text-transparent hover:text-slate-300'>{Moment(message.createdAt).format('HH:mm')}</span>
                                            </div>
                                        </div>
                                        <div className='flex w-full ml-3'>
                                            <div className='text-left mb-2'>
                                            </div>
                                            <div>
                                                <p className='text-left w-full'>
                                                    {message.content}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                );
                            } else if ( messagesList[idx - 1].userId !== message.userId ) {
                                return (
                                    <div className='flex flex-col mt-4' key={message.id}>
                                        <div className='flex mt-4'>
                                            <div>
                                                <div className="h-14 w-14">
                                                    <img className='h-14 w-14 bg-white rounded-full'/>
                                                </div>
                                            </div>
                                            <div className='flex flex-col w-full ml-3'>
                                                <div className='text-left mb-2'>
                                                    <span className='font-bold'>
                                                        { message.userId === messageUser?.id ? messageUser?.firstname : currentUser?.firstname }
                                                    </span> 
                                                    <span className='text-sm text-slate-300 mx-3'>{ message.userId === messageUser?.id ? messageUser?.faculty : currentUser?.faculty }</span>
                                                    <span className='text-sm text-slate-300'>{Moment(message.createdAt).format('HH:mm ')}</span>
                                                </div>
                                                <div>
                                                    <p className='text-left w-full'>
                                                        {message.content}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                );
                            }
                        })
                        : ""
                    }
                    <div style={{ float:"left", clear: "both" }}
                        ref={(el) => { setMessageEnd(el); }}>
                    </div>
                </div>
                <div className='flex flex-row p-2 h-12 bg-slate-700 rounded-none'>
                    <input value={message} className='w-full rounded p-2' onChange={(e) => setMessage(e.target.value)}></input>
                    <button className='m-0 w-10 ml-2 p-0' type='button' onClick={(e) => {
                            e.preventDefault;
                            setMessage("");
                            
                            if(message.length > 0) {
                                let payload = {content: message, chatId: currentChat.id , userId: currentUser.id}
                                sendMessage(payload);
                            }
                            
                            getMessageList(currentChat.id).then(() => {
                                if(messageEnd) {
                                    messageEnd.scrollIntoView({});
                                }
                            });
                        }
                    }>
                        <span className="material-symbols-outlined text-2xl bg-blue-400 text-white w-full rounded">
                            send
                        </span>
                    </button>
                </div>
                <Footer/>
            </div>
        </div>
    );
}

export default Messages;
