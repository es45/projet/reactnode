// @ts-ignore
import React, { useCallback, useState } from 'react'
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { checkLogin } from '../App';
import { login } from "../service/security.service";

const Login = () => {
    let navigate = useNavigate();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    React.useEffect((() => {
        if (checkLogin()) {
            navigate("/messages");
        }
    }), [])

    const handleChangeEmail = (e) => setEmail(e.target.value);
    const handleChangePassword = (e) => setPassword(e.target.value);

    // @ts-ignore
    const submitLogin = useCallback(async (payload) => {
        try {
            const res = await login(payload);
            const userLogged = res.data.user;
            if (userLogged.status !== 'Pending') {
                if (userLogged.isAdmin) {
                    navigate('/admin/home');
                } else {
                    navigate('/messages');
                }
            }
        } catch (e) {
            toast.error(e);
            console.log(e);
        }
    }, []);

    return (
        <div className="flex bg-gradient-to-b from-blue-600 w-full to-white min-h-screen place-content-center">
            <div className="flex flex-col w-2/3 place-content-center">
                <form>
                    <h2>Connexion</h2>
                    <div>
                        <label>Email</label>
                        <input value={email} onChange={handleChangeEmail}/>
                        <label>Mot de passe</label>
                        <input type="password" value={password} onChange={handleChangePassword}/>
                    </div>
                    <button type="button" onClick={() => submitLogin({ email, password })}>
                        Se connecter
                    </button>
                    <Link className="link" to="/register">Inscription</Link>
                    <Link to="/password-forgot">Mot de passe oublié?</Link>
                </form>
            </div>
        </div>
    );
}

export default Login;
